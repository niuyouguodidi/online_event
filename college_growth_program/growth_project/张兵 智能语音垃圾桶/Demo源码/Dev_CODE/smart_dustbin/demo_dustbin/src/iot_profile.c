/*
 * Copyright (c) 2022 zhangbing LaLa_Team.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "iot_cloud.h"
#include "iot_profile.h"
#include "cJSON.h"
#include <stdint.h>
#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "control.h"
#include "oled.h"

/**
 * @brief we make the data to a json string and send it to the iot cloud engine
 *
 */
#define delayedOpen_close 10000
#define Y_SHOW_SMART_CAN_SHOW_OPEN 4
static uint32_t Time2Tick(uint32_t ms)
{
    uint64_t ret;
    ret = ((uint64_t)ms * osKernelGetTickFreq()) / CN_MINISECONDS_IN_SECOND;
    return (uint32_t)ret;
}

/*IotProfile_Report_devStatus*/
int IotProfile_Report_ALL(int dev_status,int recyclables_Capacity,int unrecyclable_Capacity,int kitchenGarbage_Capacity,int otherRubbish_Capacity)
{
    int ret = -1;
    cJSON *root;
    char *jsonString;
    IotProfileService    service;
    IotProfileKV  kvDev_status;//dev
    IotProfileKV  kvRecyclables_Capacity;//可回收垃圾桶
    IotProfileKV  kvUnrecyclable_Capacity;//不可回收垃圾桶
    IotProfileKV  kvKitchenGarbage_Capacity;//厨余垃圾垃圾桶
    IotProfileKV  kvOtherRubbish_Capacity;//其他垃圾

    /* package the data 可回收垃圾*/
    service.eventTime = NULL;
    service.serviceID = "SmartDustbin_DATA";
    service.propertyLst = &kvDev_status;
    service.nxt = NULL;

    kvDev_status.key = "Dev_Status";
    kvDev_status.value = &dev_status;
    kvDev_status.type = IOT_PROFILE_KEY_DATATYPE_INT;
    kvDev_status.nxt = &kvRecyclables_Capacity;

    kvRecyclables_Capacity.key = "Recyclables_Capacity";
    kvRecyclables_Capacity.value = &recyclables_Capacity;
    kvRecyclables_Capacity.type = IOT_PROFILE_KEY_DATATYPE_INT;
    kvRecyclables_Capacity.nxt = &kvUnrecyclable_Capacity;

    kvUnrecyclable_Capacity.key = "Unrecyclable_Capacity";
    kvUnrecyclable_Capacity.value = &unrecyclable_Capacity;
    kvUnrecyclable_Capacity.type = IOT_PROFILE_KEY_DATATYPE_INT;
    kvUnrecyclable_Capacity.nxt = &kvKitchenGarbage_Capacity;

    kvKitchenGarbage_Capacity.key = "KitchenGarbage_Capacity";
    kvKitchenGarbage_Capacity.value = &kitchenGarbage_Capacity;
    kvKitchenGarbage_Capacity.type = IOT_PROFILE_KEY_DATATYPE_INT;
    kvKitchenGarbage_Capacity.nxt = &kvOtherRubbish_Capacity;

    kvOtherRubbish_Capacity.key = "OtherRubbish_Capacity";
    kvOtherRubbish_Capacity.value = &otherRubbish_Capacity;
    kvOtherRubbish_Capacity.type = IOT_PROFILE_KEY_DATATYPE_INT;
    kvOtherRubbish_Capacity.nxt = NULL;

    jsonString = IoTProfilePackage(&service);
    if ( NULL != jsonString) {
        RaiseLog(LOG_LEVEL_INFO, "[220206x02]jsonString:%s", jsonString);

        RaiseLog(LOG_LEVEL_INFO, "[Dustbin_tes1] cloud_reportMsg");
        printf("[Dustbin_tes1] cloud_reportMsg\n");
        ret = CLOUD_ReportMsg(jsonString);
        
        free(jsonString);
    } else {
        RaiseLog(LOG_LEVEL_ERR, "format the report message error");
    }
    return ret;
}


/*从云端获取命令*/
static int Getcommand_dustbinOpenRecyclables(CommandParamSetOpen *setOpen,cJSON *objCmd)
{
    cJSON *objPara = NULL;

    RaiseLog(LOG_LEVEL_INFO,"GetCommand_OpenR");
    printf("[Dustbin_tes1]GetCommand_OpenR");

    if (setOpen== NULL || objCmd == NULL) {
        RaiseLog(LOG_LEVEL_ERR, "NULL POINT!\n");
        return -1;
    }
    memset(setOpen, 0x00, sizeof(CommandParamSetOpen));

    if ((objPara = cJSON_GetObjectItem(objCmd, "OpenRecyclables")) == NULL) {
        RaiseLog(LOG_LEVEL_ERR, "Could not get paras in json");
        return -1;
    }
        if (0 == strcmp(cJSON_GetStringValue(objPara), "ON")) {
            RaiseLog(LOG_LEVEL_INFO,"ON");
            printf("[Dustbin_tes1]ON:OpenRecyclables\n");
            printf("[Dustbin_tes1]===open===\n");
            OledShowStr(0, Y_SHOW_SMART_CAN_SHOW_OPEN, "           ", 1);
            OledShowStr(0, Y_SHOW_SMART_CAN_SHOW_OPEN, "Dustbin:open_R ", 1);
            Dustbin_open_R();
        }
        else {
            printf("[Dustbin_tes1]===close===\n");
            OledShowStr(0, Y_SHOW_SMART_CAN_SHOW_OPEN, "           ", 1);
            OledShowStr(0, Y_SHOW_SMART_CAN_SHOW_OPEN, "Dustbin:close_R", 1);
            Dustbin_close_R();
        }
    return 0;
}

static int Getcommand_dustbinOpenUnrecyclable(CommandParamSetOpen *setOpen,cJSON *objCmd)
{
    cJSON *objPara = NULL;

    RaiseLog(LOG_LEVEL_INFO,"GetCommand_OpenU");
    printf("[Dustbin_tes1]GetCommand_OpenU");

    if (setOpen== NULL || objCmd == NULL) {
        RaiseLog(LOG_LEVEL_ERR, "NULL POINT!\n");
        return -1;
    }
    memset(setOpen, 0x00, sizeof(CommandParamSetOpen));

    if ((objPara = cJSON_GetObjectItem(objCmd, "OpenUnrecyclable")) == NULL) {
        RaiseLog(LOG_LEVEL_ERR, "Could not get paras in json");
        return -1;
    }
        if (0 == strcmp(cJSON_GetStringValue(objPara), "ON")) {
            RaiseLog(LOG_LEVEL_INFO,"ON");
            printf("[Dustbin_tes1]ON:OpenUnrecyclable\n");
            printf("[Dustbin_tes1]===open===\n");
            OledShowStr(0, Y_SHOW_SMART_CAN_SHOW_OPEN, "           ", 1);
            OledShowStr(0, Y_SHOW_SMART_CAN_SHOW_OPEN, "Dustbin:open_U ", 1);
            Dustbin_open_U();
        }
        else {
            printf("[Dustbin_tes1]===close===\n");
            OledShowStr(0, Y_SHOW_SMART_CAN_SHOW_OPEN, "           ", 1);
            OledShowStr(0, Y_SHOW_SMART_CAN_SHOW_OPEN, "Dustbin:close_U", 1);
            Dustbin_close_U();
        }
    return 0;
}

static int Getcommand_dustbinOpenKitchenGarbage(CommandParamSetOpen *setOpen,cJSON *objCmd)
{
    cJSON *objPara = NULL;

    RaiseLog(LOG_LEVEL_INFO,"GetCommand_OpenK");
    printf("[Dustbin_tes1]GetCommand_OpenK\n");

    if (setOpen== NULL || objCmd == NULL) {
        RaiseLog(LOG_LEVEL_ERR, "NULL POINT!\n");
        return -1;
    }
    memset(setOpen, 0x00, sizeof(CommandParamSetOpen));

    if ((objPara = cJSON_GetObjectItem(objCmd, "OpenKitchenGarbage")) == NULL) {
        RaiseLog(LOG_LEVEL_ERR, "Could not get paras in json");
        return -1;
    }
        if (0 == strcmp(cJSON_GetStringValue(objPara), "ON")) {
            RaiseLog(LOG_LEVEL_INFO,"ON");
            printf("[Dustbin_tes1]ON:OpenKitchenGarbage\n");
            printf("[Dustbin_tes1]===open===\n");
            OledShowStr(0, Y_SHOW_SMART_CAN_SHOW_OPEN, "           ", 1);
            OledShowStr(0, Y_SHOW_SMART_CAN_SHOW_OPEN, "Dustbin:open_K ", 1);
            Dustbin_open_K();

        }
        else {
            printf("[Dustbin_tes1]===close===\n");
            OledShowStr(0, Y_SHOW_SMART_CAN_SHOW_OPEN, "           ", 1);
            OledShowStr(0, Y_SHOW_SMART_CAN_SHOW_OPEN, "Dustbin:close_K", 1);
            Dustbin_close_K();
        }
    return 0;
}

static int Getcommand_dustbinOpenOtherRubbish(CommandParamSetOpen *setOpen,cJSON *objCmd)
{
    cJSON *objPara = NULL;

    RaiseLog(LOG_LEVEL_INFO,"GetCommand_OpenO");
    printf("[Dustbin_tes1]GetCommand_OpenO\n");

    if (setOpen== NULL || objCmd == NULL) {
        RaiseLog(LOG_LEVEL_ERR, "NULL POINT!\n");
        return -1;
    }
    memset(setOpen, 0x00, sizeof(CommandParamSetOpen));
    
    if ((objPara = cJSON_GetObjectItem(objCmd, "OpenOtherRubbish")) == NULL) {
        RaiseLog(LOG_LEVEL_ERR, "Could not get paras in json");
        return -1;
    }
        if (0 == strcmp(cJSON_GetStringValue(objPara), "ON")) {
            RaiseLog(LOG_LEVEL_INFO,"ON");
            printf("[Dustbin_tes1]ON:OpenOtherRubbish\n");
            printf("[Dustbin_tes1]===open===\n");
            OledShowStr(0, Y_SHOW_SMART_CAN_SHOW_OPEN, "           ", 1);
            OledShowStr(0, Y_SHOW_SMART_CAN_SHOW_OPEN, "Dustbin:open_O ", 1);
            Dustbin_open_O();

        }
        else {
            printf("[Dustbin_tes1]===close===\n");
            OledShowStr(0, Y_SHOW_SMART_CAN_SHOW_OPEN, "           ", 1);
            OledShowStr(0, Y_SHOW_SMART_CAN_SHOW_OPEN, "Dustbin:close_O", 1);
            Dustbin_close_O();
        }
    return 0;
}

static int DealMain_dustbinOpenRecyclables(cJSON *objCmd)//云端命令处理
{   int ret = -1;
    cJSON *objParas = NULL;
    cJSON *objPara = NULL;
    CommandParamSetOpen setOpen;
    
    RaiseLog(LOG_LEVEL_INFO,"iot :Recycly");
    printf("[Dustbin_tes1]iot :Recycly\n");

    if ((objParas = cJSON_GetObjectItem(objCmd, "paras")) == NULL) {
        RaiseLog(LOG_LEVEL_ERR, "Could not get paras in json");
        return ret;
    }
    if (Getcommand_dustbinOpenRecyclables(&setOpen, objParas) < 0) {
        RaiseLog(LOG_LEVEL_ERR, "GetCommandSetOpen failed!\n");
    }
    printf("[Dustbin_tes1]DealMain_dustbinOpenRecyclables\n");
    ret = IotProfile_CommandCallback(CLOUD_COMMAND_SETOPEN, &setOpen);
    return ret;
}

static int DealMain_dustbinOpenUnrecyclable(cJSON *objCmd){
    int ret = -1;
    cJSON *objParas = NULL;
    cJSON *objPara = NULL;
    CommandParamSetOpen setOpen;

    RaiseLog(LOG_LEVEL_INFO,"iot :Unrecyclable");
    printf("[Dustbin_tes1]iot :Unrecyclable\n");
    if ((objParas = cJSON_GetObjectItem(objCmd, "paras")) == NULL) {
        RaiseLog(LOG_LEVEL_ERR, "Could not get paras in json");
        return ret;
    }
    if (Getcommand_dustbinOpenUnrecyclable(&setOpen, objParas) < 0) {
        RaiseLog(LOG_LEVEL_ERR, "GetCommandSetOpen failed!\n");
    }
        
        printf("[Dustbin_tes1]DealMain_dustbinOpenUnrecyclable\n");
        ret = IotProfile_CommandCallback1(CLOUD_COMMAND_SETOPEN, &setOpen);

    return ret;
}

static int DealMain_dustbinOpenKitchenGarbage(cJSON *objCmd){
    int ret = -1;
    cJSON *objParas = NULL;
    cJSON *objPara = NULL;
    CommandParamSetOpen setOpen;

    RaiseLog(LOG_LEVEL_INFO,"iot :KitchenGarbage");
    printf("[Dustbin_tes1]iot :KitchenGarbage\n");
    if ((objParas = cJSON_GetObjectItem(objCmd, "paras")) == NULL) {
        RaiseLog(LOG_LEVEL_ERR, "Could not get paras in json");
        return ret;
    }
    if (Getcommand_dustbinOpenKitchenGarbage(&setOpen, objParas) < 0) {
        RaiseLog(LOG_LEVEL_ERR, "GetCommandSetOpen failed!\n");
    }

        printf("[Dustbin_tes1]DealMain_dustbinOpenKitchenGarbage\n");
        ret = IotProfile_CommandCallback2(CLOUD_COMMAND_SETOPEN, &setOpen);

    return ret;
}

static int DealMain_dustbinOpenOtherRubbish(cJSON *objCmd){
    int ret = -1;
    cJSON *objParas = NULL;
    cJSON *objPara = NULL;
    CommandParamSetOpen setOpen;

    RaiseLog(LOG_LEVEL_INFO,"iot :OtherRubbish");
    printf("[Dustbin_tes1]iot :OtherRubbish\n");
    if ((objParas = cJSON_GetObjectItem(objCmd, "paras")) == NULL) {
        RaiseLog(LOG_LEVEL_ERR, "Could not get paras in json");
        return ret;
    }
    if (Getcommand_dustbinOpenOtherRubbish(&setOpen, objParas) < 0) {
        RaiseLog(LOG_LEVEL_ERR, "GetCommandSetOpen failed!\n");
    }
        printf("[Dustbin_tes1]DealMain_dustbinOtherRubbish\n");
        ret = IotProfile_CommandCallback3(CLOUD_COMMAND_SETOPEN, &setOpen);
    
    return ret;
}

/**
 * @brief deal the message received from the queue
 *
 */

int CLOUD_CommandCallBack(const char *jsonString)
{
    cJSON *objRoot = NULL;
    cJSON *objCmdName = NULL;
    int ret = -1;
    RaiseLog(LOG_LEVEL_INFO,"CLOUD_Commandcallback");
    printf("[Dustbin_tes1]CLOUD_Commandcallback\n");
    if (jsonString == NULL) {
        return ret;
    }

    if ((objRoot = cJSON_Parse(jsonString)) == NULL) {
        RaiseLog(LOG_LEVEL_ERR, "could not parse the payload\r\n");
        goto EXIT_JSONPARSE;
    }

    if ((objCmdName = cJSON_GetObjectItem(objRoot, "command_name")) == NULL) {      //Open
        RaiseLog(LOG_LEVEL_ERR, "%s:could not get the cmd name from json\r\n");
        goto EXIT_CMDOBJ;
    }

    if (0 == strcmp(cJSON_GetStringValue(objCmdName), "Open_R")) {
        RaiseLog(LOG_LEVEL_INFO,"find:OpenRecyclables");
        printf("[S]find:OpenRecyclables\n");
        ret = DealMain_dustbinOpenRecyclables(objRoot);
    } else if(0 == strcmp(cJSON_GetStringValue(objCmdName), "Open_U")) {
        RaiseLog(LOG_LEVEL_INFO,"find:OpenUnrecyclable");
        printf("[Dustbin_tes1]find:OpenUnrecyclable\n");
        ret = DealMain_dustbinOpenUnrecyclable(objRoot);
    } else if(0 == strcmp(cJSON_GetStringValue(objCmdName), "Open_K")) {
        RaiseLog(LOG_LEVEL_INFO,"find:OpenKitchenGarbage");
        printf("[Dustbin_tes1]find:OpenKitchenGarbage\n");
        ret = DealMain_dustbinOpenKitchenGarbage(objRoot);
    }else if(0 == strcmp(cJSON_GetStringValue(objCmdName), "Open_O")) {
        RaiseLog(LOG_LEVEL_INFO,"find:OpenOtherRubbish");
        printf("[Dustbin_tes1]find:OpenOtherRubbish\n");
        ret = DealMain_dustbinOpenOtherRubbish(objRoot);
    }else {
        RaiseLog(LOG_LEVEL_ERR, "unresolved command:%d\r\n", cJSON_GetStringValue(objCmdName));
    }

EXIT_CMDOBJ:
EXIT_JSONPARSE:
    if (objRoot != NULL) {
        cJSON_Delete(objRoot);
    }
    return ret;
}



