 **# 对Gitee的使用

## 一、  Gitee的介绍

Gitee （中文名：码云 ，原名 Git@OSC）是一个版本控制和协作的代码托管平台，是开源中国推出的基于git的代码托管服务。不仅可以托管代码，还可以托管文档与图片资料。 它可以让你和其他人一起在远程或本地项目上进行协作。



## 二、  Gitee的基本使用

### 1、    注册Gitee账号

 注册网址：https://gitee.com/

![setgit1](https://gitee.com/giteehmz/smart_dustbin/raw/master/Media/git/setgit1.png)



### 2、 创建仓库

​                                     (1)点击加号：

![setgit2](https://gitee.com/giteehmz/smart_dustbin/raw/master/Media/git/setgit2.png)

​                                     (2)进入创建：

![setgit3](https://gitee.com/giteehmz/smart_dustbin/raw/master/Media/git/setgit3.png)

![setgit4](https://gitee.com/giteehmz/smart_dustbin/raw/master/Media/git/setgit4.png)

​                                    (3)创建后得到下面的结果：

![setgit5](https://gitee.com/giteehmz/smart_dustbin/raw/master/Media/git/setgit5.png)

![setgit6](https://gitee.com/giteehmz/smart_dustbin/raw/master/Media/git/setgit6.png)

这些命令配置的过程中可能会出现很多不一样的错误，所以为了不让我们这些小白容易上手，我将为你们提供一个更简便的方法！

### 3、创建好的仓库我们需要克隆到本地上就需要安装的两个软件

![setgit7](https://gitee.com/giteehmz/smart_dustbin/raw/master/Media/git/setgit7.png)

两个安装包：

 链接：https://pan.baidu.com/s/180ufZ1pwUiM9_ByzLj4j7g 
提取码：mrc5



（1)安装第一个软件：

安装过程中路径不用修改，就可以一直next下去就行。

![setgit8](https://gitee.com/giteehmz/smart_dustbin/raw/master/Media/git/setgit8.png)

（2)安装第二个软件：

该软件是用于操作第一个软件来上传代码的提交，安装该软件我们只需要一直next下去安装就行了。

（3) 配置：

​         到下面这个界面就是一个配置的过程：

![setgit9](https://gitee.com/giteehmz/smart_dustbin/raw/master/Media/git/setgit9.png)

![setgit10](https://gitee.com/giteehmz/smart_dustbin/raw/master/Media/git/setgit10.png)



​            到了这一步时需要注意看看路径：

![setgit11](https://gitee.com/giteehmz/smart_dustbin/raw/master/Media/git/setgit11.png)

![setgit12](https://gitee.com/giteehmz/smart_dustbin/raw/master/Media/git/setgit12.png)

这里弄好了，后面的就没什么了！

### 4、克隆仓库到本地

到刚刚创建仓库这复制地址：

![setgit13](https://gitee.com/giteehmz/smart_dustbin/raw/master/Media/git/setgit13.png)

![setgit14](https://gitee.com/giteehmz/smart_dustbin/raw/master/Media/git/setgit14.png)   

  在两个软件的位置空白处右击鼠标键：


![setgit15](https://gitee.com/giteehmz/smart_dustbin/raw/master/Media/git/setgit15.png)

![setgit16](https://gitee.com/giteehmz/smart_dustbin/raw/master/Media/git/setgit16.png)

![setgit17](https://gitee.com/giteehmz/smart_dustbin/raw/master/Media/git/setgit17.png)

## 三、上传代码或文件

点击Close，在这两个软件路径下会出现“test”（就是创建仓库时所起名的那个名字，只不过显示的是下面那个系统给出的名），它就是一个仓库。

![setgit18](https://gitee.com/giteehmz/smart_dustbin/raw/master/Media/git/setgit18.png)

然后点进去，点击鼠标右键，创建一个文本，文本名是你需要上传到仓库的内容名字，将需要的内容放入其中，保存后关闭。

在空白处右击鼠标可看到：

![setgit19](https://gitee.com/giteehmz/smart_dustbin/raw/master/Media/git/setgit19.png)

![setgit20](https://gitee.com/giteehmz/smart_dustbin/raw/master/Media/git/setgit20.png)

Push后，点击OK：

![setgit21](https://gitee.com/giteehmz/smart_dustbin/raw/master/Media/git/setgit21.png)

输入用户名和密码：

![setgit22](https://gitee.com/giteehmz/smart_dustbin/raw/master/Media/git/setgit22.png)


现在就可以Close了!

然后去你注册好的账户里刷新网页就可以看到你所上传的文件或代码的名字。

** 