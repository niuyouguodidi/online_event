### OpenHarmony成长计划结项说明


### 一、结项要求

- #### 结项作品提交要求
    - **应用开发类项目交付件**：
  1. Demo源码
  2. Demo演示视频（**提交至邮箱**）
  3. 说明文档
  
   - **代码注释类项目交付件**：
  1. 源码解读/说明文档 —— 注解量2000行代码以上
 
 以“**姓名+项目名**”命名文件夹，根据自己所做项目类型（应用开发或代码注释）将交付件打包整理放入文件夹内，提交**文件夹**到**growth_project**内，并在[项目登记表](项目提交登记表.md)内完成登记。 

**PS:Demo演示视频发送至邮箱 growing@mail.openharmony.io
邮件主题为姓名+项目名（例：李思同—护花使者演示视频）**
  
  请遵循开源版本规范，提前对贡献内容自查是否符合开源要求，例如：是否添加开源许可证，代码文件是否添加版权头说明等，可参考：许可证与版权规范。(https://gitee.com/openharmony/docs/blob/master/zh-cn/contribute/许可证与版权规范.md)

- ##### 结项作品提交规则

​     **代码以最终可以合进OpenHarmony主仓或相应sig仓为结项依据，通过项目结项考核即可获得对应的项目资助。**

​    Pull Request请求被合并入仓库后表示结项作品提交成功。具体方法请参考#如何在Gitee提交Pull Request（[https://gitee.com/help/articles/4128）。](https://gitee.com/help/articles/4128%EF%BC%89%E3%80%82)）

- ##### 结项作品评审规则

- [x] 代码/文档能够符合社区要求，提交到社区（知识体系或者其他的SIG）（must）主干（optional）；
- [x] 非文档类按照完成的文档能够复现所开发的功能（must）；
- [x] 应该完成题目要求的基本功能（must）,如IoT设备需要配网、联网、控制等能力展示；
- [x] 完成的题目应该得到其指导老师的认可（must）。

#### 二、激励政策

- ##### 资助激励

完成对应项目结项标准即可获得对应项目资助。

- ##### 荣誉激励
  - 项目完成即可获得证书，优秀者可获得认证奖杯；
  - 作为嘉宾受邀参加OpenHarmony社区活动；
  - 对个人及其作品进行官方宣传。

**咨询邮箱：growing@mail.openharmony.io**