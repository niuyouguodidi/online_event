# FA侧代码补充
### 通过 addActionRoute() 方法为此AbilitySlice配置一条路由规则。
```
addActionRoute(ACTION_COAL_GAS, CoalGasAbilitySlice.class.getName()); //煤气检测
```
### 在应用配置文件（config.json）中注册
```
"action.coalGas",
```
### 在CommandUtil中编写设备控制命令的获取和解析方法
```
//命令解析方法
    public static String parseCoalGasCommand(JSONObject command) {
        String beepStatus = command.getString("BeepStatus");
        if ("OFF".equals(beepStatus)) {
            return "关闭煤气泄漏检测器";
        } else {
            int threshold = command.getIntValue("Threshold");
            return "打开煤气泄漏检测器 阈值：" + threshold;
        }
    }



//命令获取方法
    /**
     * 获取煤气报警器命令
     *
     * @param isOn 是否打开
     * @return Command
     */
    public static Command getCoalGasStatusCommand(boolean isOn) {
        JSONObject params = new JSONObject();
        if (isOn) {
            params.put("BeepStatus", "ON");
        } else {
            params.put("BeepStatus", "OFF");
        }
        return new Command("SetBeep", "SmartCoSensation", JSONObject.toJSONString(params));
    }

    /**
     * 获取煤气报警器阈值
     *
     * @param threshold 报警阈值
     * @return Command
     */
    public static Command getCoalGasThresholdCommand(int threshold) {
        JSONObject params = new JSONObject();

        params.put("Threshold", threshold);
        return new Command("SetThreshold", "SmartCoSensation", JSONObject.toJSONString(params));
    }

```
### 在DeviceData类的initData方法中配置设备相关信息
```
// 煤气检测
        data.put("61de758bc7fb24029b0be6f0", new DeviceDataBean(DeviceControlAbility.ACTION_COAL_GAS,
                ResourceTable.Media_icon_fraction_smoke_online, ResourceTable.Media_icon_fraction_smoke_offline,
                command -> CommandUtil.parseCoalGasCommand(command)));

```
#### 最后须做如下修改: (1) 将OKHttpUtilsRx2类中SERVER_URL变量修改为自己的服务端api地址; (2) 将DeviceStateListener类中RABBITMQ_HOST变量修改为自己的服务端地址等信息