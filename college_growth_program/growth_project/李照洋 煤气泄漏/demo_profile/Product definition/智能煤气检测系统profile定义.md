# 智能煤气检测profile定义说明

##### 下发命令定义


```
{ 
	"service_id": "SmartCoSensation",                  // 产品服务ID，
	"command_name": "SetBeep",       			   // 命令名称，
	"paras":
	{
   		"BeepStatus": "OFF"               		   // 命令值（类型String）：ON --响铃 / OFF --关闭响铃
	}
}
```

{
	"service_id":"SmartCoSensation",          // 产品服务ID，固定
	"command_name":"SetThreshold", // 设置亮度命令名字，固定
	"paras":{
		"Threshold":100,                  // 煤气报警门限，类型(float),取值0-65535
	}
}
```
##### 属性获取定义

属性是指设备当前的一些状态，由设备端主动上报到云端

```
{ 
	"service_id": "SmartCoSensation",                  // 产品服务ID，
	"data":{
	    "BeepStatus":"ON",   // 当前状态，类型（string）,取值为“ON”（打开）、“OFF”（关闭）
   		"MQ7": 20,           // 当前煤气浓度值，类型（int）, 取值 0-65535
   		"Threshold":100,     // 当前煤气报警门限，类型（float）, 取值0-65535
	 }
}
```



##### 备注

告警消息见：[告警定义文件](./alarm_info.md)

煤气检测设备相关信息，用于写入NFC自定义数据之中

|  标签  | 名字            | 描述                                       | 定义值                      |
| :--: | ------------- | ---------------------------------------- | ------------------------ |
|  1   | IoTDA平台设备产品ID | 由IoTDA平台中获取，使用自身，标识设备产品品类。典型值24字节，小于32字节 | 61de758bc7fb24029b0be6f0 |
|  2   | NodeID        | 设备节点ID，辨识同品类中，不同的设备。典型值8字节，小于64字节        | CoDet01                 |
|  3   | DevicePwd     | 设备认证秘钥，用于设备连接IoTDA认证。典型值8字节，小于32字节       | 12345678                 |
|  4   | 配网标识          | 标识当前设备配网类型，1字节<br />0：不需要配网设备（如手表平板等自带蜂窝网络的设备）；<br />1：NAN配网 + softAP组合模式配网（能自动使用当前网络配网，不需要输入密码）;<br />2：softAP配网，连入设备热点，输入wifi密码进行配网;<br />3：ble蓝牙配网;<br />4：NAN配网 近距离贴近设备配网（能自动使用当前网络配网，不需要输入密码）； | 1                        |
|  5   | ApSSID        | 设备自身热点名，典型值12字节，小于32字节，NAN配网和softAp配网必须提供；构成一般为前缀teamX + nodeID | teamX-CoDet01           |

